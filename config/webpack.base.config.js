const webpack = require('webpack');
const path = require('path');
const merge = require("webpack-merge");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin'); // Add this in top

const APP_DIR = path.resolve(__dirname, '../src');

module.exports = env => {
    const { PLATFORM, VERSION } = env;
    return merge([
        {
            entry: ['@babel/polyfill', APP_DIR],
            // entry: './src/app.js',
            // output: {
            //     filename: 'bundle.js',
            //     path: path.resolve(__dirname, 'dist')
            // },
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        use: {
                            loader: 'babel-loader'
                        }
                    },
                    {
                        test: /\.(scss)$/,
                        use: [
                            PLATFORM === 'production' ? MiniCssExtractPlugin.loader :
                            {
                                // Adds CSS to the DOM by injecting a `<style>` tag
                                loader: 'style-loader'
                            },
                            {
                                // Interprets `@import` and `url()` like `import/require()` and will resolve them
                                loader: 'css-loader'
                            },
                            {
                                // Loader for webpack to process CSS with PostCSS
                                loader: 'postcss-loader',
                                options: {
                                    plugins: function () {
                                        return [
                                            require('autoprefixer')
                                        ];
                                    }
                                }
                            },
                            {
                                // Loads a SASS/SCSS file and compiles it to CSS
                                loader: 'sass-loader'
                            }
                        ]
                    }
                ]
            },
            plugins: [
                new HtmlWebpackPlugin({
                    template: './src/index.html',
                    filename: './index.html',
                    inject: true,
                    minify: {
                        collapseWhitespace: true,
                        collapseInlineTagWhitespace: true,
                        minifyCSS: true,
                        minifyURLs: true,
                        minifyJS: true,
                        removeComments: true,
                        removeRedundantAttributes: true
                    }
                }),
                new webpack.DefinePlugin({
                    'process.env.VERSION': JSON.stringify(env.VERSION),
                    'process.env.PLATFORM': JSON.stringify(env.PLATFORM)

                }),
                new CopyWebpackPlugin([{ from: 'src/static' }]), // Add this in the plugins section
            ]
        }
    ]);
}